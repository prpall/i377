package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import dao.OrderDao;
import model.Order;
import javax.validation.Valid;
import java.util.List;

@RestController
public class PostController {



    @Autowired
    private OrderDao dao;

    @GetMapping("orders")
    public List<Order> getPosts() {
        return dao.getOrders();
    }

    @GetMapping("orders/{id}")
    public Order getOrderById(@PathVariable("id") Long id) {
        return dao.getOrderById(id);
    }

    @PostMapping("orders")
    public Order save(@RequestBody @Valid Order order) {
        return dao.save(order);
    }
}
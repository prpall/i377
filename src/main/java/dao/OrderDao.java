package dao;

import model.Order;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class OrderDao {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public Order save(Order order) {
        if (order.getId() == null) {
            em.persist(order);
        } else {
            em.merge(order);
        }
        return order;
    }

    /*
    @Transactional
    public List<Item> getItemsById(Long id) {
        List<Item> items = new ArrayList<>();
        TypedQuery<Order> query1 = em.createQuery(
                "select o from Order o where o.id = :id",
                Order.class
        );
        query1.setParameter("id", id);

        Order foo = query1.getSingleResult();
        System.out.println("<------------------------------->");
        for (Item i : foo.getOrderRows()) {
            items.add(i);
        }
        return items;
    }
    */

    @Transactional
    public Order getOrderById(Long id) {
        TypedQuery<Order> query = em.createQuery(
                "select o from Order o where o.id = :id",
                Order.class
        );
        query.setParameter("id", id);
        return query.getSingleResult();
    }

    @Transactional
    public List<Order> getOrders() {
        TypedQuery<Order> query = em.createQuery(
                "select o from Order o",
                Order.class
        );
        return query.getResultList();
    }

}

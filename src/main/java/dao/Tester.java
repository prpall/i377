package dao;

import conf.DbConfig;
import model.Item;
import model.Order;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Tester {

    public static void main(String[] args) throws Exception {

        ConfigurableApplicationContext ctx =
                new AnnotationConfigApplicationContext(DbConfig.class);

        OrderDao dao = ctx.getBean(OrderDao.class);
        Order order1 = new Order("a123");
        order1.addRows(new Item("Motherboard", 10, 6));
        order1.addRows(new Item("PSU", 10, 3));
        dao.save(order1);

        Order order2 = new Order("b5743");
        order2.addRows(new Item("CPU", 4, 10));
        dao.save(order2);

        System.out.println("********************************");

        System.out.println(dao.getOrderById(1L));
        System.out.println(dao.getOrderById(2L));

        System.out.println("All orders: \n\n\n\n");
        System.out.println(dao.getOrders());

        ctx.close();
    }
}
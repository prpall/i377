package model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "orders")
public class Order {

    @Column(name = "order_number")
    private String orderNumber;

    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "my_seq")
    @SequenceGenerator(
            name = "my_seq",
            sequenceName = "order_sequence",
            allocationSize = 1)
    private Long id;

    @Valid
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(
            name = "order_rows",
            joinColumns=@JoinColumn(name = "orders_id",
                    referencedColumnName = "id")
    )
    private List<Item> orderRows = new ArrayList<>();

    public Order() {
    }

    public Order(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Order(String orderNumber, List<Item> orderRows) {
        this.orderNumber = orderNumber;
        this.orderRows = orderRows;
    }

    public void addRows(Item item) {
        orderRows.add(item);
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderNumber='" + orderNumber + '\'' +
                ", id=" + id +
                ", orderRows=" + orderRows +
                '}';
    }
}

package model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class Item {
    @Column(name = "item_name")
    private String itemName;

    @Column(name = "quantity")
    @NotNull
    @Min(value = 1)
    private Integer quantity;

    @Column(name = "price")
    @NotNull
    @Min(value = 1)
    private Integer price;

    @Override
    public String toString() {
        return "Item{" +
                "itemName='" + itemName + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }
}
